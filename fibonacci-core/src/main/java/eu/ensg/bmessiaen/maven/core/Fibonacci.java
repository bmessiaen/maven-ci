package eu.ensg.bmessiaen.maven.core;

public class Fibonacci {

    public static long calculate(long n) {

        if (n < 0) return 0;

        return f(n);
    }

    private static long f(long n) {

        if ( n == 0 || n == 1 ) return n;

        long i = 2;
        long f0 = 0;
        long f1 = 1;
        long temp;


        while (i <= n) {

            temp = f1;
            f1 = f0 + f1;
            f0 = temp;

            i++;
        }

        return f1;
    }
}