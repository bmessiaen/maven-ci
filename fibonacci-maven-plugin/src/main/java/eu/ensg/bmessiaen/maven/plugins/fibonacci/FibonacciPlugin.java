package eu.ensg.bmessiaen.maven.plugins.fibonacci;


import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import eu.ensg.bmessiaen.maven.core.Fibonacci;

@Mojo(name = "fibonacci")
public class FibonacciPlugin extends org.apache.maven.plugin.AbstractMojo {

    @Parameter(property = "value")
    private String strval;

    public void execute() {

        long val = Long.parseLong(strval);
        long res = Fibonacci.calculate(val);

        System.out.println(res);
    }
}